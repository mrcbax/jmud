/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

/**
 * Implements a Command for dropping an Item.
 */
class DropCommand extends Command {
  private String toDrop;

  /**
   * Constructor for objects of class DropCommand
   * @param itemName Name of desired Item
   */
  DropCommand(String itemName) {
    toDrop = itemName;
  }

  /**
   * Executes a drop command.
   * @return A message to Interpreter to be displayed on stdout.
   */
  String execute() {
    String toReturn="";
    GameState gs = GameState.sharedInstance();
    if (toDrop.equals("")) {
      toReturn="Drop what?";
    }
    else {
      if (gs.adventurerHasItem(toDrop)) {
        Item i = gs.removeItemFromAdventurersInventory(toDrop);
        gs.getAdventurersCurrentRoom().addItem(i);
        CannedResponses cr = new CannedResponses();
        toReturn = cr.getResponse("DROP", toDrop);
      }
      else {
        toReturn="You don't have the item \""+toDrop+"\".";
      }
    }
    return toReturn;
  }
}
