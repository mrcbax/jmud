/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

public class Exit
{

  private String direction;
  private Room source;
  private Room destination;
    private boolean isLocked;

  /**
   * A Room and its associated exits.
   * @param  dir           Direction.
   * @param  src           Source Room.
   * @param  dest          Destination Room.
   */
  public Exit(String dir, Room src, Room dest)
  {
    direction=dir;
    source=src;
    destination=dest;
  }

  /**
   * Returns a description of possible exits for a Room.
   * @return A String describing possible exits.
   */
  String describe()
  {
    return "You can go "+direction+" to "+destination.getTitle()+".";
  }

  /**
   * Gets the direction a traveler wishes to travel.
   * @return A String that is the direction.
   */
  String getDir()
  {
    return direction;
  }

  /**
   * Gets the Room that would like to know its exits.
   * @return A source Room.
   */
  Room getSrc()
  {
    return source;
  }

  /**
   * Gets a possible destination for the current source Room.
   * @return A destination Room.
   */
  Room getDest()
  {
    return destination;
  }
    boolean isLocked()
    {
        return isLocked;
    }
    void setLocked(boolean l)
    {
        isLocked=l;
    }
}
