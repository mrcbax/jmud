/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

import java.util.Hashtable;
/**
 * An Item object represents an Item in the Bork game
 */
public class Item {

    private String name;
    private int weight;
    private boolean producesLight;
    private int brightness;
    private Hashtable<String,Action> actions;

  /**
   * Constructor for objects of class Item
   * @param inName Name of Item
   * @param inWeight Weight of Item
   * @param producesLight Whether or not an item produces light.
   * @param brightness how bright an item is.
   */
  Item (String inName, int inWeight, boolean producesLight, int brightness) {
    this.name = inName;
    this.weight = inWeight;
    this.producesLight = producesLight;
    setBrightness(brightness);
    this.actions = new Hashtable<String,Action>();
  }

  /**
   * Constructor for objects of class Item
   * @param inName Name of Item
   * @param inWeight Weight of Item
   */
  Item (String inName, int inWeight) {
    this.name = inName;
    this.weight = inWeight;
    this.producesLight = false;
    setBrightness(0);
    this.actions = new Hashtable<String,Action>();
  }

  /**
   * returns the brightness of the item.
   * @return brightness
   */
  int getBrightness() {
    return this.brightness;
  }

  /**
   * sets the brightness of an object.
   * @param int brightness the brightness.
   */
  void setBrightness(int brightness) {
    if (brightness > 100){
      this.brightness = 100;
    } else if(brightness < 0) {
      this.brightness = 0;
    } else {
      this.brightness = brightness;
    }
  }

  boolean doesProduceLight() {
    return this.producesLight;
  }

  void setDoesProduceLight(boolean producesLight) {
    this.producesLight = producesLight;
  }

  /**
   * Adds an Action to this Item's Action collection
   * @param actionName   Desired name of action
   * @param actionResult Message that this action returns
   */
  void addAction(Action a)
  {
      actions.put(a.toString(), a);
  }
  /**
   * toString for objects of class Item
   * @return Name of this Item
   */
  public String toString()
  {
      return name;
  }
  /**
   * Generates a result message based on action name
   * @param  action        String representation of an action
   * @return               The resulting message based on action name
   */
  public String getMessageForAction(String action)
  {
      if (actions.get(action) != null)
      {
          return actions.get(action).getResult();
      }
      return "You can't "+action+" a "+name+".";
  }
  /**
   * Compares two Item objects
   * @param i             Item object to compare to this Item object
   * @return              True or false based on whether or not the passed Item is
   * equivalent to this Item.
   */
  public boolean equals(Item i)
  {
      return i.toString().equals(toString());
  }
}
