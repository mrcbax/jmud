/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class MovementCommand extends Command
{
  private String direction;

  /**
   * Constructor for objects of class MovementCommand
   * @param dir Direction to move
   */
  MovementCommand(String dir)
  {
      direction=dir;
  }
  /**
  * Executes a MovementCommand.
  * @return A message to Interpreter to be displayed on stdout.
  * @see Interpreter
  */
  String execute()
  {
      String toReturn="";
      GameState gs=GameState.sharedInstance();
      Room nextRoom=gs.getAdventurersCurrentRoom().leaveBy(direction);
      if (nextRoom != null)
      {
          gs.setAdventurersCurrentRoom(nextRoom);
          CannedResponses cr = new CannedResponses();
          toReturn = cr.getResponse("MOVEMENT", gs.getAdventurersCurrentRoom().describe());
      }
      else
      {
          toReturn = "There is no exit in that direction.";
      }
      return toReturn;
  }
}
