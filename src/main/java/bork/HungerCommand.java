/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

/**
 * Implements a Command for handling the viewing of Hunger
 */
class HungerCommand extends Command{

  /**
   * Constructor for objects of class HungerCommand
   */
  HungerCommand(){

  }

  /**
   * Executes a HungerCommand
   * @return A message to Interpreter to be displayed on stdout.
   */
  String execute(){
    GameState gs = GameState.sharedInstance();
    CannedResponses cr = new CannedResponses();
    return cr.getResponse("HUNGER", ""+gs.getHunger());
  }
}
