/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

public class IllegalDungeonFormatException extends Exception {

  /**
   * Default constructor for IllegalDungeonFormatException objects
   */
  public IllegalDungeonFormatException() {

  }
  /**
   * Constructor for IllegalDungeonFormatException objects
   * @param  inMessage     Exception message
   */
  public IllegalDungeonFormatException(String inMessage) {
    super(inMessage);
  }
}
