/**
 * @author cmistr
 * @version 3.2.0
 */

package bork;

class SaveCommand extends Command {
  private String saveName;

  /**
   * Creates a SaveCommand class.
   * @param inSaveName The path to save to.
   */
  SaveCommand(String inSaveName) {
      saveName=inSaveName;
  }

  /**
   * Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   * @see Interpreter
   */
  String execute() {
      String toReturn="";
      GameState gs = GameState.sharedInstance();
      CannedResponses cr = new CannedResponses();
      if (saveName.equals(""))
      {
          String fileName = gs.getDungeon().getFileName();
          fileName=fileName.substring(0, fileName.indexOf("."));
          gs.store(fileName);
          String saveFileName = gs.getDungeon().getFileName();
          saveFileName = saveFileName.substring(0, saveFileName.indexOf(".zork")) + ".sav";
          toReturn = cr.getResponse("SAVE", "Saved as "+saveFileName);
      }
      else
      {
          gs.store(saveName);
          toReturn = cr.getResponse("SAVE", "Saved as "+saveName+".sav");
      }
      return toReturn;
  }
}
