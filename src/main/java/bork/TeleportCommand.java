/**
 * @author cbaxter
 * @version 3.2.0
 */

package bork;

class TeleportCommand extends Command{
  private Room src;
  private Room dest;

  /**
   *Creates a TeleportCommand class.
   */
  TeleportCommand(Room src, Room dest){
    this.src = src;
    this.dest = dest;
  }

  /**
   *Executes the command and returns a displayable String for Interpreter.
   * @return A human readable String.
   */
  String execute(){
    GameState gs = GameState.sharedInstance();
    gs.setAdventurersCurrentRoom(this.dest);
    CannedResponses cr = new CannedResponses();
    return cr.getResponse("TELEPORT", dest.getTitle());
  }
}
